# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('option_1', models.CharField(max_length=400)),
                ('option_1_image', models.ImageField(upload_to=b'option_images')),
                ('option_2', models.CharField(max_length=400)),
                ('option_2_image', models.ImageField(upload_to=b'option_images')),
                ('updated', models.DateTimeField(auto_now=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
