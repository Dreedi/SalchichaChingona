# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profiles', '0003_auto_20150601_0248'),
        ('questions', '0002_auto_20150601_0308'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAnswerQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=400)),
                ('question', models.ForeignKey(to='questions.Question')),
                ('user', models.ForeignKey(to='user_profiles.UserProfile')),
            ],
        ),
    ]
