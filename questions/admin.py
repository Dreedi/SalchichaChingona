from django.contrib import admin

from . import models

@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
	list_display = ('id', 'option_1', 'option_2', 'value')

@admin.register(models.UserAnswerQuestion)
class UserAnswerQuestionAdmin(admin.ModelAdmin):
 	list_display = ('id', 'user', 'question', 'answer', 'updated_score')