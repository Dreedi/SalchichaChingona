from django.db import models

from user_profiles.models import UserProfile

class Question(models.Model):
	option_1 = models.CharField(max_length=400)
	option_2 = models.CharField(max_length=400)
	updated = models.DateTimeField(auto_now=True)
	created = models.DateTimeField(auto_now_add=True)

	def value(self):
		return self.id

	def __unicode__(self):
		return "%s o %s" %(self.option_1, self.option_2)

class UserAnswerQuestion(models.Model):
	user = models.ForeignKey(UserProfile)
	question = models.ForeignKey(Question)
	answer = models.CharField(max_length=400)
	updated_score = models.BooleanField(default=False)

	def update_scores(self):
		if not self.updated_score:
			if self.answer == self.question.option_1:
				self.user.score_1 += self.question.value()
			elif self.answer == self.question.option_2:
				self.user.score_2 += self.question.value()
			self.updated_score = True
			self.save()
			return self.user.save()
		else:
			return None