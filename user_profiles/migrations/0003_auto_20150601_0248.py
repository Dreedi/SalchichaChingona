# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profiles', '0002_auto_20150601_0232'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='score_1',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='score_2',
            field=models.BigIntegerField(default=0),
        ),
    ]
