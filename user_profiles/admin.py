from django.contrib import admin

from . import models

@admin.register(models.UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'user', 'score_1', 'score_2', 'show_avatar')