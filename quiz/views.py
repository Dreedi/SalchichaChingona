from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404, render
from django.views import generic

from questions.models import Question, UserAnswerQuestion
from user_profiles.models import UserProfile

class QuizView(generic.TemplateView):
	template_name = 'quiz_home.html'

	def get_context_data(self):
		context = super(QuizView, self).get_context_data()
		context['title'] = 'Salchicha Chingona'
		context['meta_description'] = 'Answer all the questions bellow and see with who you are compatible'
		if not self.request.user.is_anonymous():
			user_profile_object = UserProfile.objects.get_or_create(user=self.request.user)
			if not user_profile_object[0].name or not user_profile_object[0].avatar:
				user_profile_object[0].fill_user_data()
			context['user'] = user_profile_object
		try:
			question = Question.objects.filter(useranswerquestion__question__isnull=True)[0]
		except:
			question = None
		if question:
			context['question'] = question
		else:
			context['message'] = 'There is no more questions brouu'
		return context

	def post(self, request):
		context = super(QuizView, self).get_context_data()
		question_id = request.POST.get('question', None)
		answer_option = request.POST.get('answer', None)
		question = get_object_or_404(Question, id=question_id)
		if answer_option not in ['option_1', 'option_2']:
			return HttpResponseForbidden()
		else:
			answer = str()
			if answer_option == 'option_1':
				answer = question.option_1
			elif answer_option == 'option_2':
				answer = question.option_2
			user = UserProfile.objects.get(user=request.user)
			user_answer_question_object = UserAnswerQuestion(user=user, question=question, answer=answer)
			user_answer_question_object.save()
			user_answer_question_object.update_scores()
			try:
				question = Question.objects.filter(useranswerquestion__question__isnull=True)[0]
			except:
				question = None
			if question:
				context['question'] = question
			else:
				context['message'] = 'There is no more questions brouu'
			print context
			return render(self.request, self.template_name, context)