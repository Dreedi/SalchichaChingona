from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = patterns(
	'',
	url(
		r'^$',
		login_required(views.QuizView.as_view(), login_url='/login/'),
		name='home'
	),
)