from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(
    	r'^admin/',
    	include(admin.site.urls)
    ),
    url(
    	'',
    	include('social.apps.django_app.urls',namespace='social')
    ),
    url(
    	'',
    	include('social_app.urls', namespace='social_app')
    ),
    url(
    	'',
    	include('quiz.urls', namespace='quiz')
    ),
]
