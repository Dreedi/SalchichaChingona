from django.contrib.auth import logout as auth_logout
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views import generic


def session_logout(request):
	auth_logout(request)
	return redirect(reverse('quiz:home'))

class SessionLoginView(generic.TemplateView):
	template_name = 'facebook_login.html'

	def get_context_data(self):
		context = super(SessionLoginView, self).get_context_data()
		context['title'] = 'Salchicha Chingona Login'
		context['meta_description'] = 'Login into the best quiz ever and see who shares the same answers with you'
		return context