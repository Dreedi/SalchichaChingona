from django.conf.urls import patterns, url, include

from . import views

urlpatterns = patterns(
	'',
	url(
		r'^logout/$',
		views.session_logout,
		name='logout'
	),
	url(
		r'^login/$',
		views.SessionLoginView.as_view(),
		name='login'
	),
)